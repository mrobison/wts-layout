(function($){
	$.fn.extend({
		jqSlidr: function(options) {
			var defaults = {
				classes: {
					slide:      'jqSlide',
					title:      'jqTitle',
					img:        'jqImage',
					content:    'jqContent',
					wheel:      'jqWheel',
					wheelul:    'jqWheelUl',
					wheelli:    'jqWheelLi',
					carousel:   'jqCarousel',
					carouselul: 'jqCarouselUl',
					carouselli: 'jqCarouselLi',
					current:    'jqCurrent',
				},
				interval: 6000, /* Time between transition */
				speed: 1500, /* Transition speed */
				effect: 'fade', /* Carousel Transition Effect */
				display: 'wheel-carousel', /* could be carousel-wheel */
				current: 1,
				currentSelector: ':nth-child(3)',
				auto: true,
				wheelEffect: 'down', /* Wheel effect */
				wheelEasing: 'easeInOutQuad',
				carouselEffect: 'down', /* Carousel effects:
						'up'
						'down',
						'left',
						'right',
						'fade'
				*/
				carouselEasing: 'easeInOutQuad',
			};
			var options = $.extend(true, defaults, options);
			/*
			 * Here we set up each slider on the page...
			 */
			return this.each(function() {
				var o          = options;
				var obj        = $(this);
				var internals  = obj.clone();
				var count      = internals.size();
				var carousel   = null;
				var wheel      = null;
				var wheelul    = null;
				var timeoutID  = null;
				var spinning   = false;
				var compass    = null;
				// clear the display
				obj.children().detach();

				// handle display....

				if (o.display === 'wheel-carousel'
					|| o.display === 'wheel'
				) {
					wheel = $('<div class="'+o.classes.wheel+
						'"></div>');
					obj.append(wheel);
				}
				if (o.display === 'wheel-carousel'
					|| o.display === 'carousel-wheel'
					|| o.display === 'carousel'
				) {
					carousel = $('<div class="'+o.classes.carousel+
						'"></div>');
					obj.append(carousel);
					if (o.display === 'carousel-wheel') {
						wheel = $('<div class="'+o.classes.wheel+
							'"></div>');
						obj.append(wheel);
					}
				}

				if (wheel !== null) {
					wheelul = $('<ul class="'+o.classes.wheelul+'"></ul>');
					internals.find('.'+o.classes.title).each(function(i){
						$('<li class="'+o.classes.wheelli+'">'+
							$(this).html()+'</li>').
							data('id', i).
							click(activateSlide).
							appendTo(wheelul);
					});
					count = wheelul.children().size();
					wheel.append(wheelul);
					$(o.currentSelector,wheelul).addClass(o.classes.current);
				}
				if (carousel !== null) {
					carouselul = $('<ul class="'+o.classes.carouselul+'"></ul>');
					internals.find('.'+o.classes.slide).each(function(i){
						$('<li class="'+o.classes.carouselli+'">'+
							$(this).html()+'</li>').
							data('id', i).
							click(activateSlide).
							appendTo(carouselul);
					});
					count = carouselul.children().size();
					carousel.append(carouselul);
					if (o.carouselEffect === 'up' || o.carouselEffect === 'down') {
						compass = 'top';
						carouselul.css({
							'height':'9999px',
							'position':'relative',
							'top':0,
							'display':'block'
						});
						$('li',carouselul).each(function(){
							$(this).css({
								'position':'relative'
							});
						});

					} else if (o.carouselEffect === 'left'
						|| o.carouselEffect === 'right'
					) {
						compass = 'left';
						carouselul.css({
							'width':'9999px',
							'position':'relative',
							'top':0,
							'display':'inline'
						});
						$('li',carouselul).each(function(){
							$(this).css({
								'position':'relative'
							});
						});

					} else if (o.carouselEffect === 'fade') {
						compass = 'fade';
						$('li',carouselul).each(function(){
							$(this).css({
								'position':'absolute'
							});
						});
					}
					var i = $('.'+o.classes.carouselli+o.currentSelector,
						carouselul).index();
					if (i > 0) {
						$('.'+o.classes.carouselli,carouselul).slice(0,i).
							appendTo(carouselul);
					}
				}
				function rotateCarousel(dir,dist,callback) {
					var height = compass==='top'?carousel.height():carousel.left();
					var css    = {};
					if (dir === 'up') {
						$('.'+o.classes.carouselli,carouselul).
							slice(0,dist).
							each(function(){
								$(this).clone().appendTo(carouselul);
							}
						);
						height = '-'+(dist*height);

					} else {
						$($('.'+o.classes.carouselli,carouselul).
							slice(-dist).get().reverse()).each(function(){
								$(this).clone().prependTo(carouselul);
							}
						);
						css[compass] = '-'+(dist*height)+'px';
						carouselul.css(css);
						height = 0;
					}
					css[compass] = height;
					carouselul.animate(
						css,
						o.speed, 
						o.carouselEasing,
						function(){
							if (dir === 'up') {
								$('.'+o.classes.carouselli,carouselul).
									slice(0,dist).
									each(function(){
										$(this).detach();
									}
								);
							} else {
								$('.'+o.classes.carouselli,carouselul).
									slice(-dist).
									each(function(){
										//console.log('detach',this);
										$(this).detach();
									}
								);
							}
							css[compass] = 0;
							carouselul.css(css);
							if (typeof callback !== 'undefined') {
								callback();
							}
						}
					);
				}
				function activatgeHorse() {
				}
				function rotateWheel(dir,dist,next,callback) {
					var height = $(':first-child', wheelul).outerHeight(true);
					if (dir === 'up') {
						$(':lt('+dist+')',wheelul).each(
							function(i){
								i++;
								$(this).clone().
									appendTo(wheelul).
									click(activateSlide).
									removeClass(o.classes.current).
									hide().
									fadeIn(o.speed*i/dist);
								//$(this).fadeTo(o.speed*i/dist,0);
							}
						);
						height = '-'+(dist*height);
					} else {
						$($(':gt('+(count-dist-1)+')',wheelul).
							get().reverse()).each(
							function(i){
								i++;
								$(this).clone().
									prependTo(wheelul).
									click(activateSlide).
									removeClass(o.classes.current).
									hide().
									fadeIn(o.speed*i/dist);
								//$(this).fadeTo(o.speed*i/dist,0);
							}
						);
						wheelul.css({'top':'-'+(dist*height)+'px'});
						height = 0;
					}
					$('.'+o.classes.current,wheelul).each(
						function(){
							$(this).removeClass(o.classes.current,o.speed);
						}
					);
					if (typeof next === 'string') {
						next = $(next,wheelul);
					}
					next.addClass(o.classes.current,o.speed);
					if (o.auto) {
						rotateCarousel(o.carouselEffect,dist);
					} else {
						rotateCarousel(dir,dist);
					}
					wheelul.animate(
						{'top': height},
						o.speed, 
						o.wheelEasing,
						function(){
							if (dir === 'up') {
								$(':lt('+dist+')',wheelul).each(
									function(){
										$(this).detach();
									}
								);
							} else if (dir === 'down') {
								$(':gt('+(count-1)+')',wheelul).each(
									function(){
										$(this).detach();
									}
								);
							}
							wheelul.css({'top':0});
							o.current = next.data('id');
							if (typeof callback !== 'undefined') {
								callback();
							}
						}
					);
					
				}
				function activateNextSlide() {
					if (spinning) return;
					spinning = true;
					var next = null;
					if (o.wheelEffect === 'up') {
						if (o.currentSelector !== ':last-child') {
							next = $(o.currentSelector, wheelul).next();
						} else {
							next = o.currentSelector;
						}
					} else if (o.wheelEffect === 'down') {
						if (o.currentSelector !== ':first-child') {
							next = $(o.currentSelector, wheelul).prev();
						} else {
							next = o.currentSelector;
						}
					}
					rotateWheel(
						o.wheelEffect,
						1,
						next,
						function() {
							if (o.auto) {
								timeoutID = setTimeout(
									activateNextSlide,
									o.interval
								);
							}
							spinning = false;
						}
					);
						
				}
				function activateSlide() {
					if (spinning) return;
					clearTimeout(timeoutID);
					o.auto    = false;
					spinning  = true;
					var index = $(this).index();
					var curr  = $(o.currentSelector,wheelul).index();
					if (index === curr) {
						spinning = false;
						return;
					}
					rotateWheel(
						index>curr?'up':'down',
						Math.abs(index-curr),
						$(this),
						function() {
							spinning = false;
						}
					);
				}
				
				activateNextSlide();
				
			});
		}
	});
})(jQuery);
